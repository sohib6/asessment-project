

$(document).ready(function () {

    $('#exam_questions_answers').submit(function (event) {
        event.preventDefault();

        //var inputs = $('#exam_questions_answers :input');
        //
        //console.log(inputs);

        var json = {
            "ExamName" : $("input[name='exam_name']").val()
        };

        json["Questions"] = [];

        var questions = $("input[name^=question_]").each(function (index) {

            index = parseInt($(this).attr('name').split("_").pop())-1;

            var count_opt = $("input[name^=opt][name$=_"+(index+1)+"]").size();
            //if(true) {
            //    console.log("input[name^=opt][name$=_"+(index+1)+"]");
            //    console.log([count_opt , $(this)]);
            //    return;
            //}
            if(count_opt ==2){
                count_opt="tf";
            }else {
                count_opt ="mcq"
            }

            var answers = [] ;
            $("input[name^=opt][name$=_"+(index+1)+"]").each(function (i) {
                answers.push($(this).val());
            });

            //var correct = .split('_').pop();
            questionObj = {
                type : count_opt,
                title: $('input[name=question_name_'+ (index +1) +  ']').val(),
                correct_answer : parseInt($("input[name=correct_answer_"+(index+1)+"]:checked").val().match("opt([0-9])_[0-9]")[1])-1,
                answers : answers
            };

            json.Questions.push(questionObj);




        });
        json['_token'] = $(this).find('input[name=_token]').val();

        var url = $(this).attr('action');
        //console.log(url);

        $.post(url,json, function ( data, textStatus,  jqXHR ) {
                console.log();
            if(jqXHR.status = 200){
                alert(data.message);
                window.location = location.protocol + "//" + location.host + "/teacher/exam/"+data.Exam_id;
            }

        });


        //console.log(ss);

        //console.log(json)

    });


});