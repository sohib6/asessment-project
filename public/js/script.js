$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};


function generate_from_template(template_name, loop, id) {
    var source = $(template_name).html();
    var template = Handlebars.compile(source);
    //noinspection JSDuplicatedDeclaration


    for (i = 0; i < loop; i++) {

        var context = {'questionId': id};
        var html = template(context);
        $('#examquestions').append(html);
        id++;
    }

    return id;

}

function generate_from_bank_template(questionBank, id) {
    var source = $("#mcq-bank-template").html();
    var template = Handlebars.compile(source);
    //noinspection JSDuplicatedDeclaration

    $(questionBank).each(function (key, value) {
        var context = {'questionId': id,
            'questionName': value.name,
            'option_correct': value.option_correct,
            'option_1':value.option_1,
            'option_2':value.option_2,
            'option_3':value.option_3,
            'option_4':value.option_4
        };
        var html = template(context);
        $('#examquestions').append(html);


        id++;
    });



    return id;

}

$(document).ready(function () {

    Handlebars.registerHelper('ifCond', function(v1, v2, options) {
        if(v1 === v2) {
            return options.fn(this);
        }
        return options.inverse(this);
    });

    $('#exam_questions_answers').hide();

    $("#examGenerator").submit(function (event) {

        $('#examquestions').empty();


        var form_data = $(this).serializeObject();
        var mcq = form_data.mcq;
        var tf = form_data.tf;


        // mcq for loop
        // mcq 2
        var id = generate_from_template('#mcq-template', mcq, 1);
        //console.log(stop);

        id = generate_from_template('#tf-template', tf, id);

        var checkboxValues = [];
        $('input[name="bank"]:checked').map(function () {
            var value = $(this).val().split("_");
            //console.log(value[0]);
            checkboxValues.push({
                exam: value[0],
                question: value[1]

            })
        });
        checkboxValues = {
            _token: $('input[name="_token"]').val(),
            questions: checkboxValues
        };
        console.log(checkboxValues);

        $.post('/teacher/course/' + $('input[name=course_id]').val() + '/getQuestions', checkboxValues, function (data, textStatus, jqXHR) {
            generate_from_bank_template(data, id);
        });

        $('#exam_questions_answers').show();

        event.preventDefault();

    });


    $('#exam_questions_answers').submit(function (event) {
        event.preventDefault();

        //var inputs = $('#exam_questions_answers :input');
        //
        //console.log(inputs);

        var json = {
            "ExamName": $("input[name='exam_name']").val()
        };

        json["Questions"] = [];

        var questions = $("input[name^=question_]").each(function (index) {

            var count_opt = $("input[name^=opt][name$=_" + (index + 1) + "]").size();

            if (count_opt == 2) {
                count_opt = "tf";
            } else {
                count_opt = "mcq"
            }

            var answers = [];
            $("input[name^=opt][name$=_" + (index + 1) + "]").each(function (i) {
                answers.push($(this).val());
            });

            //var correct = .split('_').pop();
            questionObj = {
                type: count_opt,
                title: $('input[name=question_name_' + (index + 1) + ']').val(),
                correct_answer: parseInt($("input[name=correct_answer_" + (index + 1) + "]:checked").val().match("opt([0-9])_[0-9]")[1]) - 1,
                answers: answers
            };

            json.Questions.push(questionObj);


        });
        json['_token'] = $(this).find('input[name=_token]').val();

        var url = $(this).attr('action');
        //console.log(url);

        $.post(url, json, function (data, textStatus, jqXHR) {

            window.location = "/teacher/course/" + $('input[name=course_id]').val();


        });


        //console.log(ss);

        //console.log(json)

    });

    $('#clearBtn').on('click', function () {


        $('#examquestions').empty();

    });
});