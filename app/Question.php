<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Question
 *
 * @property-read \App\Exam $exam
 * @property integer $id
 * @property string $name
 * @property string $option_correct
 * @property string $option_1
 * @property string $option_2
 * @property string $option_3
 * @property integer $exam_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Question whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Question whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Question whereOptionCorrect($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Question whereOption1($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Question whereOption2($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Question whereOption3($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Question whereExamId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Question whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Question whereUpdatedAt($value)
 */
class Question extends Model
{
    //
    public function exam(){
        return $this->belongsTo('App\Exam');
    }

    public function submission(){
        return $this->hasMany('App\Submission');
    }
}
