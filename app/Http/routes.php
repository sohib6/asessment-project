<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\User;
use App\Course;
use App\Exam;
use App\Question;
use App\Submission;

// test
Route::get('main/{name}', ['as' => 'main', function ($name) {
    return $name;
}]);
Route::get('test', function () {

    return redirect()->route('main', 'fahad')->with('name', 'fahad');

});


Route::get('/', function () {

    // get currant user
    $user = Auth::user();

    if ($user && $user->type == 'teacher') {
//      return 'you are teacher';
        return redirect()->route('teacherHome');
    } elseif ($user && $user->type == 'student') {
        return redirect()->route('studentHome');

    } else {
        return view('home', [
            'username' => $user ? $user->name : "New User"

        ]);

    }


});
Route::get('/home' , function () {
    return redirect('/');
});

Route::get('/teacher', ['as' => 'teacherHome', function () {
//    dd(Auth::user()->coursesTaught()->get());
    $users = User::where('type', '=', 'student')->get();
//    dd();
    return view('teacher.home', [
        'users' => $users,
    ]);
}]);

Route::post('/create_course', function () {
    $input = Input::all();
//        dd($input);

    $course = new Course();
    $course->name = $input['course_name'];
    $course->user_id = Auth::user()->id;
    $course->save();
    $course->students()->sync($input['course_students']);

    return redirect()->route('teacherHome');


});

Route::get('/teacher/course/{course_id}',[ 'as'=>'teacherCourse',function ($course_id) {
    # code...
//    dd(User::find(1)->courses[0]->name);

    $course = Course::find($course_id);


    return view('teacher.course', ['course' => $course]);
}]);

Route::get('/teacher/course/{course_id}/newExam', function ($course_id) {
    # code...
    $course = Course::find($course_id);
    return view('newExam', ['course_id' => $course_id , 'course'=>$course]);
});

Route::post('teacher/course/{course_id}/getQuestions', function ($course_id) {
    $questions = Input::get('questions');
    $response = [];
    foreach($questions as $question){
        $response[] = Exam::find($question['exam'])->questions->find($question['question']);
    }


    return Response::json($response);
});

Route::post('/teacher/course/{course_id}/newExam', function ($course_id) {

    $input = Input::all();
    $exam = new Exam();
    $exam->name = $input["ExamName"];
    $exam->course_id = $course_id;
    $exam->save();

    foreach ($input['Questions'] as $question) {
        $q = new Question();
        $q->name = $question['title'];
        $q->option_correct = $question['answers'][$question['correct_answer']];

        //delete correct ansower
        unset($question['answers'][$question['correct_answer']]);
        $question['answers'] = array_values($question['answers']);
        // add all the answer's to the database

        if ($question['type'] == "tf") {
            $q->option_1 = $question['answers'][0];

        } else {
//            dd($question['answers']);
            $q->option_1 = $question['answers'][0];
            $q->option_2 = $question['answers'][1];
            $q->option_3 = $question['answers'][2];

        }
        $q->exam_id = $exam->id;
        $q->save();
    }


//    return $input["ExamName"];
});

Route::get('/teacher/exam/{exam_id}', function ($exam_id) {

    $exam = Exam::find($exam_id);
    //  dd($exam->questions);

    // calculate marks

    $marks = [];
    foreach ($exam->course->students as $student) {

        //student name
        $student->name;
        // student total marks in exam


        $answers = $exam->questions->map(function ($question, $key) use ($student) {
            $userSubmission = $question->submission->where('user_id', $student->id)->first();
            if ($userSubmission == null) {
                return false;
            } else {
                return $question->option_correct == $userSubmission->answer;
            }
        });

        $trueAnswers = 0;
        foreach ($answers as $answer) {
            if ($answer)
                $trueAnswers++;
        }

//        $examsMarks['exam'] = ['name' => $exam->name, 'id' => $exam->id, 'correct' => $trueAnswers, 'total' => count($answers)];
        $examsMarks[$student->id] = ['student Name' => $student->name, 'name' => $exam->name, 'id' => $exam->id, 'correct' => $trueAnswers, 'total' => count($answers)];


    }
    return view('teacher.exam', ['exam' => $exam, 'marks' => $examsMarks]);


});

Route::post('/teacher/editExam/{exam_id}', ['as' => 'editExam', function ($exam_id) {


    $input = Input::all();
    $exam = Exam::find($exam_id);

    $exam->name = $input['ExamName'];
    $exam->save();

    $questions = Question::whereExamId($exam->id);
    $questions->delete();


    foreach ($input['Questions'] as $question) {
        $q = new Question();
        $q->name = $question['title'];
        $q->option_correct = $question['answers'][$question['correct_answer']];

        //delete correct ansower
        unset($question['answers'][$question['correct_answer']]);
        $question['answers'] = array_values($question['answers']);
        // add all the answer's to the database

        if ($question['type'] == "tf") {
            $q->option_1 = $question['answers'][0];

        } else {
//            dd($question['answers']);
            $q->option_1 = $question['answers'][0];
            $q->option_2 = $question['answers'][1];
            $q->option_3 = $question['answers'][2];

        }
        $q->exam_id = $exam->id;
        $q->save();
    }

    return response()->json(['message' => 'Exam successfully edited', 'Exam_id' => $exam->id]);

}]);
Route::get('/teacher/{exam_id}/{student_id}/answers', ['as' => "teacherAnswers", function ($exam_id, $student_id) {

    $exam = Exam::find($exam_id);

        $answers = $exam->questions->map(function ($question, $key) use ($student_id) {
        $correctAnswer = $question->option_correct;
        $userAnswer = $question->submission->where('user_id', intval($student_id));

        if (!$userAnswer->isEmpty()) {
        
         $userAnswer = $question->submission->where('user_id', intval($student_id))->values()[0];


        }

        if ($userAnswer == null ) {
            $answersText = [

                'binaryValue' => $correctAnswer == false,
                'answers' => ['question' => $question->name, 'correct' => $correctAnswer, 'user' => 'user has no ansower']

            ];

        }else {
            try{
            $answersText = [

                'binaryValue' => $correctAnswer == $userAnswer,
                'answers' => ['question' => $question->name, 'correct' => $correctAnswer, 'user' => $userAnswer->answer]

            ];
        }catch(ErrorException $e){

   $answersText = [

                'binaryValue' => $correctAnswer == false,
                'answers' => ['question' => $question->name, 'correct' => $correctAnswer, 'user' => 'user has no ansower']

            ];
            }
        }

        return $answersText;
    });




    $trueAnswers = 0;
    $examsMarks = [];

    foreach ($answers->all() as $answer) {
        if ($answer['binaryValue'])
            $trueAnswers++;
        $examsMarks['answers'][] = $answer['answers'];
    }
//    dd();$answers->
    $examsMarks['exam'] = ['name' => $exam->name, 'correct' => $trueAnswers, 'total' => count($answers)];

    return view('teacher.answers', ['data' => $examsMarks]);


}]);
Route::post('/teacher/course/{course_id}/deleteStudent', function ($course_id) {
    $student_id =Input::get('studentId');
    $course = Course::find($course_id);
    $course->students()->detach($student_id);

    return redirect()->route('teacherCourse', [$course->id]);

});



Route::get('/student', ['as' => 'studentHome', function () {
    # code...
    $userCourses = Course::whereNotIn("id",Auth::user()->courses->lists('id'))->get();


    return view('student.home', ['user' => Auth::user(), 'courses'=>$userCourses]);

}]);

Route::post('/student/registerCourse',function () {
    $courseId = Input::only(['course_id'])['course_id'];
    Course::find($courseId)->students()->attach(Auth::user()->id);

    return redirect('/');

});
Route::get('/student/exam/{exam_id}', function ($exam_id) {
    $exam = Exam::find($exam_id);
    return view('student.exam', ['exam' => $exam]);
});
Route::get('/student/course/{course_id}', ['as' => "studentCourse", function ($course_id) {
    # code...
//    dd(User::find(1)->courses[0]->name);
//    dd(Auth::user()->id);
    // calculate marks


    $course = Course::find($course_id);
    $examsMarks = [];

    foreach ($course->exams as $exam) {

        $answers = $exam->questions->map(function ($question, $key) {
            $userSubmission = $question->submission->where('user_id', Auth::user()->id);
            if($question->submission == null || $question->submission->isEmpty() || $userSubmission->isEmpty())
                return false;
            return $question->option_correct == $question->submission->where('user_id', Auth::user()->id)->first()->answer;
        });
//        $trueAnswers = $answers->all()->where(true);
        $answers->all();

        $trueAnswers = 0;
        foreach ($answers as $answer) {
            if ($answer)
                $trueAnswers++;
        }

        $examsMarks['exam'] = ['name' => $exam->name, 'id' => $exam->id, 'correct' => $trueAnswers, 'total' => count($answers)];
    }


    return view('student.course', ['course' => $course, 'marks' => $examsMarks]);
}]);

Route::post('/student/exam/{exam_id}', function ($exam_id) {

    $input = Input::all();

    foreach ($input['questions'] as $key => $value) {

        $submition = new Submission();
        $submition->user_id = Auth::user()->id;
        $submition->question_id = $key;
        $submition->answer = $value;
        $submition->save();
    }

    return redirect()->route('studentCourse', ['course_id' => Exam::find($exam_id)->course->id]);


});

Route::get('/student/exam/{exam_id}/answers', ['as' => "studentAnswers", function ($exam_id) {

    $exam = Exam::find($exam_id);


    $answers = $exam->questions->map(function ($question, $key) {
        $correctAnswer = $question->option_correct;
        $userAnswer = $question->submission->where('user_id', intval(Auth::user()->id));

        if (!$userAnswer->isEmpty()) {
        
         $userAnswer = $question->submission->where('user_id', intval(Auth::user()->id))->values()[0];


        }
        if ($userAnswer == null ) {
            $answersText = [

                'binaryValue' => $correctAnswer == false,
                'answers' => ['question' => $question->name, 'correct' => $correctAnswer, 'user' => 'user has no ansower']

            ];

        }else {
            try{
            $answersText = [

                'binaryValue' => $correctAnswer == $userAnswer,
                'answers' => ['question' => $question->name, 'correct' => $correctAnswer, 'user' => $userAnswer->answer]

            ];
        }catch(ErrorException $e){

   $answersText = [

                'binaryValue' => $correctAnswer == false,
                'answers' => ['question' => $question->name, 'correct' => $correctAnswer, 'user' => 'user has no ansower']

            ];
            }
        }

        return $answersText;
    });


    $trueAnswers = 0;
    $examsMarks = [];

    foreach ($answers->all() as $answer) {
        if ($answer['binaryValue'])
            $trueAnswers++;
        $examsMarks['answers'][] = $answer['answers'];
    }
//    dd();$answers->
    $examsMarks['exam'] = ['name' => $exam->name, 'correct' => $trueAnswers, 'total' => count($answers)];

    return view('student.answers', ['data' => $examsMarks]);


}]);


// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');
