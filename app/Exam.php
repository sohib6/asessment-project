<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Exam
 *
 * @property-read \App\Course $course
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Question[] $questions
 * @property integer $id
 * @property string $name
 * @property integer $course_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Exam whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Exam whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Exam whereCourseId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Exam whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Exam whereUpdatedAt($value)
 */
class Exam extends Model
{
    //
    // many to one
    public function course(){
        return $this->belongsTo('App\Course');
    }

    public function questions(){
        return $this->hasMany('App\Question');
    }
}
