<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Submission
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $question_id
 * @property string $answer
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Submission whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Submission whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Submission whereQuestionId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Submission whereAnswer($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Submission whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Submission whereUpdatedAt($value)
 */
class Submission extends Model
{
    //

    public function user(){
        return $this->belongsTo('App\user');
    }

    public function Question(){
        return $this->belongsTo('App\Question');
    }
}
