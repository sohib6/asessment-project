<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Course
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $students
 * @property-read \App\User $teacher
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Exam[] $exams
 * @property integer $id
 * @property string $name
 * @property integer $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereUpdatedAt($value)
 */
class Course extends Model
{
    //

    public function students()
    {
        return $this->belongsToMany('App\User');
    }

    public function teacher(){
        return $this->belongsTo('App\User','user_id');
    }

    public function exams(){
        return $this->hasMany('App\Exam');
    }


}
