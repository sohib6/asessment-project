<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for ($i = 0; $i < 10; $i++) {
            DB::table('users')->insert([
                'name' => str_random(10),
                'email' => str_random(10) . '@fahad.com',
                'type' => (rand(0, 1) == 1) ? "teacher" : "student",
                'password' => bcrypt('secret'),
            ]);
        }
    }
}
