@extends('layouts.layout')


@section('title', 'Home Page')



@section('content')
    <form action="/create_course" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <label>
            <h3>Create a new Course</h3>
            <input class="text_input" type="text" name="course_name" value="">
        </label>

        <h3>Choose Your Students</h3>

        <div id="checkbox">

            @foreach($users as $user)
                <label>

                    <input type="checkbox" name="course_students[]" value="{{$user->id}}"/>
                    {{$user->id}}  -- {{$user->name}}
                    <br/>
                </label>
            @endforeach

            @if($users->count() == 0)
                Sadly there no registered students Yet!
            @endif

        </div>

        <input class="submit_btn" placeholder="Type Your Course Name Here" type="submit" name="submit" value="submit">

    </form>


    <h3>Your Cources</h3>


    <table class="CSSTableGenerator">
        <tr>
            <td>
                Course Id - Course Name
            </td>
            <td>
                Number Of Exams
            </td>

            <td>
                Number Of Students
            </td>

        </tr>


        @foreach(Auth::user()->coursesTaught()->get() as $course)
            <tr>

                <td>
                    <a href="/teacher/course/{{$course->id}}"> {{$course->id}} -  {{$course->name}}</a>
                </td>
                <td>
                    {{$course->exams->count()}}
                </td>
                <td>
                    {{$course->students->count()}}
                </td>
            </tr>
        @endforeach

    </table>




@endsection
