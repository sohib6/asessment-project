@extends('layouts.layout')


@section('title', 'Home Page')



@section('content')


    <h1> {{ $data['exam']['name'] }} Marks </h1>


    <table>

        <tr>
            <td>Question</td>
            <td>Correct Answer</td>
            <td>{{Auth::user()->name}} Answer</td>
        </tr>

        @foreach($data['answers'] as $answer)


            <tr @if($answer['correct'] == $answer['user'] )  class="correct-answer" @else class="false-answer" @endif >
                <td>{{$answer['question']}}</td>
                <td>{{$answer['correct']}}</td>
                <td>{{$answer['user']}}</td>
            </tr>


        @endforeach

        <tr>
            <td>
                Correct
            </td>
            <td>
                false
            </td>
            <td>Total</td>
        </tr>

        <tr>
            <td>
                {{$data['exam']['correct']}}
            </td>
            <td>
                {{$data['exam']['total'] - $data['exam']['correct']}}

            </td>
            <td>
                {{$data['exam']['total']}}
            </td>
        </tr>
    </table>





@endsection
