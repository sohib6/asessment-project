@extends('layouts.layout')


@section('title', 'Home Page')



@section('content')




    <h1>{{$course->name}}</h1>

    <div class="simpleTabs">
        <ul class="simpleTabsNavigation">
            <li><a href="#">Course Exams </a></li>
            <li><a href="#">Course Students</a></li>
        </ul>
        <div class="simpleTabsContent">

            <table class="center" style="text-align:center;">
                @foreach($course->exams as $exam)
                    <tr>
                        <td>
                            <a href="/teacher/exam/{{$exam->id}}"> Exam : {{$exam->name}} </a>
                        </td>

                    </tr>
                @endforeach


            </table>

            <button class="submit_btn" onclick="window.location='/teacher/course/{{$course->id}}/newExam';"
                    type="button" name="button"> New Exam
            </button>

        </div>
        <div class="simpleTabsContent">

            <table class="center">

                <tr>
                    <td>
                        Student Id -- Student Name
                    </td>
                    <td>
                        Actions
                    </td>
                </tr>

                @foreach($course->students as $student)
                <tr>
                    <td>
                        {{$student->id}} -- {{$student->name}}
                    </td>
                    <td>
                        <form method="post" action="/teacher/course/{{$course->id}}/deleteStudent">
                            {{csrf_field()}}
                            <input type="hidden" name="studentId" value="{{$student->id}}">
                            <input type="submit" value="delete X">
                        </form>
                    </td>
                </tr>
                    @endforeach



            </table>

        </div>

    </div>



@endsection
