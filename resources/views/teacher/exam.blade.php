@extends('layouts.layout')


@section('title', 'Home Page')


@section('scripts')

    <script type="text/javascript" src="{{URL::asset('js/editExam.js')}}"></script>

@endsection


@section('content')


    <div class="simpleTabs">
        <ul class="simpleTabsNavigation">
            <li><a href="#">Submissions</a></li>
            <li><a href="#">Edit Questions </a></li>
        </ul>
        <div class="simpleTabsContent">

            <table class=".CSSTableGenerator">

                <tr>
                    <td>
                        Student Name

                    </td>

                    <td>Student Mark</td>
                </tr>
                @foreach($marks as $studentId => $studentExam)
                <tr>

                    <td>
                        <a href="{{route('teacherAnswers',['exam_id'=> $exam->id, "student_id" => $studentId ])}}"> ${{$studentExam['student Name']}}</a>
                    </td>
                    <td>
                        {{$studentExam['correct']}} / {{$studentExam['total']}}
                    </td>

                </tr>
                    @endforeach



            </table>

        </div>

        <div class="simpleTabsContent">


            <form class="form-style-1" id="exam_questions_answers" action="{{URL::route('editExam',['exam_id'=> $exam->id])}}">

                {{csrf_field()}}
                <li>
                    <label>
                        Exam Name
                        <input class="field-long" name='exam_name' value="{{$exam->name}}" type="text">
                    </label>
                </li>

                @foreach($exam->questions as $question)


                    <li>
                        <label>
                            <input name="question_name_{{$question->id}}" value="{{$question->name}}" type="text">
                        </label>
                    </li>
                    <li>
                        <label>
                            Option 1 <input type="radio" name="correct_answer_{{$question->id}}"
                                            value="opt1_{{$question->id}}" checked>
                            <input name="opt1_{{$question->id}}" value="{{$question->option_correct}}" type="text">
                        </label>
                    </li>

                    <li>
                        <label>
                            Option 2 <input type="radio" name="correct_answer_{{$question->id}}"
                                            value="opt2_{{$question->id}}">
                            <input name="opt2_{{$question->id}}" value="{{$question->option_1}}" type="text">
                        </label>
                    </li>

                    @unless($question->option_2 == null)
                        <li>
                            <label>
                                Option 3 <input type="radio" name="correct_answer_{{$question->id}}"
                                                value="opt3_{{$question->id}}">
                                <input name="opt3_{{$question->id}}" value="{{$question->option_2}}" type="text">
                            </label>
                        </li>
                    @endunless

                    @unless($question->option_3 == null)

                        <li>
                            <label>
                                Option 4 <input type="radio" name="correct_answer_{{$question->id}}"
                                                value="opt4_{{$question->id}}">
                                <input name="opt4_{{$question->id}}" value="{{$question->option_3}}" type="text">
                            </label>
                        </li>
                    @endunless



                @endforeach

                <input type="submit" value="edit exam">

            </form>


        </div>

    </div>


@endsection
