@extends('layouts.layout')


@section('title', 'Home Page')



@section('content')
<div class="grid-30">

<center>
   <div id="login-form">
      <form method="post" action="/auth/register">
         <table align="center" width="100%" border="0">
           {!! csrf_field() !!}

            <tr>
               <td><input type="text" name="name" value="{{ old('name') }}" placeholder="User Name" required /></td>
            </tr>
            <tr>
               <td><input type="email" name="email" value="{{ old('email') }}" placeholder="Your Email" required /></td>
            </tr>
            <tr>
               <td><input type="password" name="password" placeholder="Your Password" required /></td>
            </tr>

            <tr>
               <td><input type="password" name="password_confirmation" placeholder="retype your password" required /></td>
            </tr>

            <tr>
               <td>Select user type : <br>
               <input type="radio" name="type" value="teacher" checked>Teacher  <input type="radio" name="type" value="student">Student
               </td>
            </tr>

            <tr>
               <td><button type="submit" name="submit">Sign Me Up</button></td>
            </tr>


            <tr>
               <td><a href="/auth/login">Sign In Here</a></td>
            </tr>
         </table>
      </form>
   </div>
</center>
</div>

<div class="grid-70">
    <h1 id="welcomeMsg">Welcome to Online Evalution and Assesments System</h1>
</div>
@endsection
