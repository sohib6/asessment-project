@extends('layouts.layout')


@section('title', 'Home Page')



@section('content')

<div class="grid-30">

     <div id="login-form">
        <form method="post" action="/auth/login" >
           <table align="center" width="100%" border="0">
             {!! csrf_field() !!}

              <tr>
                 <td><input type="text" name="email" placeholder="Your Email" value="{{ old('email') }}" required /></td>
              </tr>
              <tr>
                 <td><input type="password" name="password" placeholder="Your Password" required /></td>
              </tr>

              <!-- <tr>
                 <td>Select user type : <br>
                 <input type="radio" name="type" value="teacher" >Teacher  <input type="radio" name="type" value="student">Student
                 </td>
              </tr> -->

              <tr>
                <td>
                <input type="checkbox" name="remember"> Remember Me
              </td>
              </tr>

              <tr>
                 <td><button type="submit" name="submit">Sign In</button></td>
              </tr>
              <tr>
                 <td><a href="/password/email">Did you forgot your password?</a></td>
              </tr>
              <tr>
                 <td><a href="/auth/register">Sign Up Here</a></td>
              </tr>
           </table>
        </form>
     </div>
</div>
<div class="grid-70">
    <h1 id="welcomeMsg">Welcome to Online Evalution and Assesments System</h1>
</div>





@endsection
