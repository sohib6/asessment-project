@extends('layouts.layout')


@section('title', 'Home Page')



@section('content')


    <h1>{{$exam->name}}</h1>

    <form class="exam"  action="/student/exam/{{$exam->id}}" method="post">
        {{csrf_field()}}
        <input type="hidden" name="std_id" value="{{Auth::user()->id}}" >
        <ol>
            @foreach($exam->questions as $question)
                <li><p class="question">{{$question->name}}</p>


                    <ul class="answers">

                        <li><input type="radio" name="questions[{{$question->id}}]" value="{{$question->option_correct}}"
                                   id="q{{$question->id}}a"><label
                                    for="q{{$question->id}}a">{{$question->option_correct}}</label></li>

                        <li><input type="radio" name="questions[{{$question->id}}]" value="{{$question->option_1}}"
                                   id="q{{$question->id}}b"><label
                                    for="q{{$question->id}}b">{{$question->option_1}}</label></li>

                        @if($question->option_2)
                            <li><input type="radio" name="questions[{{$question->id}}]" value="{{$question->option_2}}"
                                       id="q{{$question->id}}c"><label
                                        for="q{{$question->id}}c">{{$question->option_2}}</label></li>
                        @endif


                        @if($question->option_3)
                            <li><input type="radio" name="questions[{{$question->id}}]" value="{{$question->option_3}}"
                                       id="q{{$question->id}}d"><label
                                        for="q{{$question->id}}d">{{$question->option_3}}</label></li>
                        @endif
                    </ul>


                </li>
            @endforeach
        </ol>

        <input type="submit" name="submit" id="submitexam" value="submit your answers">
    </form>



@endsection
