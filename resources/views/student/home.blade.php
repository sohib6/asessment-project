@extends('layouts.layout')


@section('title', 'Home Page')



@section('content')


    <h1>Hi {{$user->name}} , Welcome to your page </h1>

    <table class="center" class="CSSTableGenerator" style="text-align:center;">
        <tr>
            <td>Course Id - Course Name</td>
            <td>Course Teacher</td>
        </tr>
        @foreach($user->courses as $course)
            <tr>
                <td>
                    <a href="student/course/{{$course->id}}">{{$course->id}} - {{$course->name}}</a>
                </td>
                <td>{{$course->teacher->name}}</td>
            </tr>
        @endforeach


    </table>

    @unless($courses == null||$courses->isEmpty())
        <h4 style="text-align: center">Or Register in A course</h4>
        <form class="form-style-1" action="/student/registerCourse" method="post">
            {{ csrf_field() }}
            Course Name : <select name="course_id" class="form-control field-select">
                @foreach($courses as $course)
                    <option value="{{$course->id}}">{{$course->id}} - {{$course->name}} ({{$course->teacher->name}})</option>
                @endforeach
            </select>

            <input class="form-inline" type="submit">

        </form>
    @endunless

@endsection
