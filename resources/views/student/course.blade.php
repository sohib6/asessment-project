@extends('layouts.layout')


@section('title', 'Home Page')



@section('content')




    <h1>{{$course->name}}</h1>

    <div class="simpleTabs">
        <ul class="simpleTabsNavigation">
            <li><a href="#">Course Exams </a></li>
            <li><a href="#">Course Marks</a></li>
        </ul>
        <div class="simpleTabsContent">

            <table class="center" style="text-align:center;">
                <tr>
                    <td>Exams</td>
                </tr>
                @foreach($course->exams as $exam)
                    <tr>

                        <td>
                            <a href="/student/exam/{{$exam->id}}">   <button type="button" name="button"> Go to {{$exam->name}}</button></a>
                        </td>

                    </tr>
                @endforeach


            </table>



        </div>
        <div class="simpleTabsContent">

            <table class="center">

                <tr>
                    <td>
                        Exam Name
                    </td>
                    <td>
                        Exam marks
                    </td>
                </tr>
                @foreach($marks as $mark => $markInfo)

                    <tr>
                        <td>
                            <a href="{{route('studentAnswers',['exam_id'=> $markInfo['id'] ])}}">{{$markInfo['name']}}</a>
                        </td>
                        <td>
                            {{$markInfo['correct']}} / {{$markInfo['total']}}
                        </td>
                    </tr>

                @endforeach

            </table>

        </div>

    </div>



@endsection
