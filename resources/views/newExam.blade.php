@extends('layouts.layout')


@section('title','Create new exam')

@section('scripts')

    <script type="text/javascript" src="{{URL::asset('js/script.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('/js/handlebars-v4.0.5.js')}}"></script>
    <script id="mcq-template" type="text/x-handlebars-template">

        <li>
            <label>Q@{{questionId}}:
                <input name="question_name_@{{questionId}}" type="text">
            </label>
        </li>
        <li>
            <label>
                Option 1 <input type="radio" name="correct_answer_@{{questionId}}" value="opt1_@{{questionId}}">
                <input name="opt1_@{{questionId}}" type="text">
            </label>
        </li>

        <li>
            <label>
                Option 2 <input type="radio" name="correct_answer_@{{questionId}}" value="opt2_@{{questionId}}">
                <input name="opt2_@{{questionId}}" type="text">
            </label>
        </li>

        <li>
            <label>
                Option 3 <input type="radio" name="correct_answer_@{{questionId}}" value="opt3_@{{questionId}}">
                <input name="opt3_@{{questionId}}" type="text">
            </label>
        </li>

        <li>
            <label>
                Option 4 <input type="radio" name="correct_answer_@{{questionId}}" value="opt4_@{{questionId}}">
                <input name="opt4_@{{questionId}}" type="text">
            </label>
        </li>




    </script>

    <script id="tf-template" type="text/x-handlebars-template">
        <li>
            <label>Q@{{questionId}}:
                <input name="question_name_@{{questionId}}" type="text">
            </label>
        </li>
        <li>
            <label>
                Option 1 <input type="radio" name="correct_answer_@{{questionId}}" value="opt1_@{{questionId}}">
                <input name="opt1_@{{questionId}}" type="text">
            </label>
        </li>

        <li>
            <label>
                Option 2 <input type="radio" name="correct_answer_@{{questionId}}" value="opt2_@{{questionId}}">
                <input name="opt2_@{{questionId}}" type="text">
            </label>
        </li>


    </script>

    <script id="mcq-bank-template" type="text/x-handlebars-template">

        <li>
            <label>Q@{{questionId}}:
                <input name="question_name_@{{questionId}}" value="@{{questionName}}" type="text">
            </label>
        </li>
        <li>
            <label>
                Option 1 <input checked="checked"  type="radio" name="correct_answer_@{{questionId}}" value="opt1_@{{questionId}}">
                <input name="opt1_@{{questionId}}" value="@{{option_correct}}" type="text">
            </label>
        </li>

        <li>
            <label>
                Option 2 <input type="radio" name="correct_answer_@{{questionId}}" value="opt2_@{{questionId}}">
                <input name="opt2_@{{questionId}}" value="@{{option_1}}" type="text">
            </label>
        </li>
        @{{#ifCond option_2 ""}}

        <li>
            <label>
                Option 3 <input type="radio" name="correct_answer_@{{questionId}}" value="opt3_@{{questionId}}">
                <input name="opt3_@{{questionId}}"  value="@{{option_2}}" type="text">
            </label>
        </li>
        @{{/ifCond}}
        @{{#ifCond option_3 ""}}

        <li>
            <label>
                Option 4 <input type="radio" name="correct_answer_@{{questionId}}" value="opt4_@{{questionId}}">
                <input name="opt4_@{{questionId}}" value="@{{option_3}}" type="text">
            </label>
        </li>
        @{{/ifCond}}




    </script>
@endsection

@section('content')


    <form id="examGenerator" class="form-style-1">
        {{csrf_field()}}
        <li>
            <label>

                What is the number of Multiple Choice Questions?

                <select name="mcq" class="field-long">
                    @for($i = 0; $i <=30; $i ++)
                        <option value="{{$i}}">{{$i}}</option>
                    @endfor
                </select>
            </label>
        </li>
        <li>
            <label>

                What is Number of True and False Questions?

                <select name="tf" class="field-long">
                    @for($i = 0 ; $i <= 30; $i ++)
                        <option value="{{$i}}">{{$i}}</option>
                    @endfor
                </select>

            </label>
        </li>
        <input type="hidden" name="course_id" value="{{$course_id}}">
        <li>
            <label > Question Bank

                <div id="checkbox" style="text-align: left">

                    @foreach($course->exams as $exam)
                        @foreach($exam->questions as $question)
                        <label>

                            <input type="checkbox" name="bank" value="{{$exam->id}}_{{$question->id}}"/>
                              {{$exam->name}} --> Q: {{$question->name}} @if($question->option_3 == null) (TF) @else {{"(MCQ)"}} @endif
                            <br/>
                        </label>
                            @endforeach
                    @endforeach

                    @if($course->exams->count() == 0)
                        Sadly there no registered students Yet!
                    @endif

                </div>

            </label>

        </li>

        <li>
            <input type="submit" value="Generate Form">
        </li>
        <li>
            <input id="clearBtn"type="button" value="clear">
        </li>
    </form>


    <form id="exam_questions_answers" class="form-style-1" method="post" action="/teacher/course/{{$course_id}}/newExam" >
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <li>
            <label>
                Exam Name
                <input class="field-long" name='exam_name' type="text">
            </label>
        </li>

        <div id="examquestions">

        </div>

        <li>
            <input type="submit" value="Submit Questions">
        </li>
    </form>



@endsection