<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="x-ua-compatible" content="ie=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />

<title> assesments system </title>
<link rel="stylesheet" href="{{ URL::asset('css/style.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ URL::asset('css/unsemantic-grid-responsive.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ URL::asset('css/simpletabs.css') }}" type="text/css" />
<script type="text/javascript" src="{{ URL::asset('js/simpletabs_1.3.packed.js') }}"></script>
{{--<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>--}}
    <script type="text/javascript" src="{{URL::asset("js/jquery-2.1.4.min.js")}}"></script>
    @yield('scripts')
</head>
<body>
<div id="header">
	<div id="left">
    <label> Student Assesments System  -- @yield('title') </label>
    </div>
    <div id="right">
    	<div id="content">


				@if (Auth::user())
        	<a href="/auth/logout">Sign Out</a>
				@else
				<a href="/auth/login">Sign In</a>
				@endif

        </div>
    </div>
</div>

<div class="grid-container" id="body">


  @yield('content')


</div>

</body>
</html>
